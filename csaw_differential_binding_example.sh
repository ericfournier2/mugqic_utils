#!/bin/bash
#SBATCH --time=04:00:00
#SBATCH --account=def-amnou
#SBATCH --mem=32G
#SBATCH --cpus-per-task 8
#SBATCH --error stderr.log
#SBATCH --output stdout.log

# Soumets ce script avec la commande:
#    sbatch -D `pwd` csaw_differential_binding_example.sh


module load nixpkgs/16.09 gcc/7.3.0 r/3.6.1

# Tu vas avoir besoin d'installer plusieurs librairies dans R pour faire fonctionner
# ce script. Tu peux les installer avec les lignes suivantes:
# install.packages("BiocManager")
# install.packages("argparser")
# install.packages("ggplot2")
# install.packages("dplyr")
# BiocManager::install("BiocParallel")
# BiocManager::install("csaw")
# BiocManager::install("edgeR")
# BiocManager::install("statmod")

# Dans un fichier bash, finir une ligne avec le symbole \ (backslash) indique 
# que la commande se poursuit à la ligne suivante. Ajoute des options au  
# metagene en rajoutant des lignes à la commande Rscript, et en mettant
# un backslash à la fin de toutes les lignes sauf la dernière.

# Mets le bon nom de régions où tu veux faire du binding différentiel,
# ainsi que le bon fichier contraste. Le fichier contraste doit avoir le même
# format que le fichier design (Lignes = échantillons, colonnes = contrastes,
# 2 = Traitement, 1=Référence, 0=ignorer):
Rscript ~/mugqic_utils/csaw_differential_binding.R \
    -r TSS-NAMED_GENES-NON_OVERLAPPING-1000.txt \
    -c input/csaw_design_mugqic_utils.txt
#
# Par défaut les reads sont étendus à 200, mais tu peux changer cette valeur avec la ligne suivante:
#    --extend 200 \
#
# Inclus cette ligne pour changer le nombre de processeurs en parrallèle.
# C'est inutile de mettre plus de processeur qu'il n'y a de bam.
# Il faut avoir au minimum 4Go de mémoire par processeur.
# Il ne faut pas oublier de changer le nombre de CPU et la mémoire dans l'en-tête SLURM.
#    --thread 8 \
#
# Modifies cette ligne si la sortie du pipelien de mugqic n'est pas dans le
# sous-dossier output/pipeline.
#    --mugqic output/pipeline \
#
# Modifies cette ligne si tu veux utiliser un autre readset. Tu peux créer des 
# fichiers readset qui sont des sous-ensembles de tes échantillons pour faire
# des métagènes avec seulement certains échantillons.
#    --sample raw/readset.txt \
#
# Par défaut le pipeline filtre les bams pour enlever les alignements ambigüs,
# ce qui fais que tu vas avoir une couverture à 0 dans les régions répétées.
# Tu peux utiliser les bam non-filtrés en utilisant l'option suivante:
#    --unfiltered \
#
# Par défaut, la p-value minimale pour qu'un gène soit considéré significatif 
# est 0.05. Cette valeur peut être changée:
#    --pthreshold 0.01 \
