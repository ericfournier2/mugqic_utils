#!/bin/bash

# Set argument default values.
# Where are the MUGQIC results stored?
MUGQIC_DIR=ERROR

# What is the filetype suffix for input files?
INPUTSUFFIX=.sorted.dup.bam
DEFAULTSUFFIX=TRUE

# What should the filetype suffix for output files be?
OUTPUTSUFFIX=""

# Under which account should the computations be performed?
RAP=ERROR

# Should we use MOAB or SLURM?
SLURM=TRUE

# Should we overwrite existing files?
OVERWRITE=FALSE

# Should we convert chromosome names from Ensembl to UCSC
# naming convention?
UCSC=FALSE

# Should we generate bigwigs from the sample alignments, or
# the readset alignments? Useful for targeting unfiltered
# bam files.
READSET=FALSE

# How much should we extend reads?
EXTEND=200
DEFAULTEXTEND=TRUE

# Should we skip the job submission step?
NOSUBMIT=FALSE

# Print help text?
HELP=FALSE

# Read command line arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -d|--mugqicdir)
    MUGQIC_DIR=$2
    shift
    ;;
    -s|--inputsuffix)
    INPUTSUFFIX=$2
    DEFAULTSUFFIX=FALSE
    shift
    ;;
    -o|--outputsuffix)
    OUTPUTSUFFIX=$2
    shift
    ;;    
    -r|--rapid)
    RAP=$2
    shift
    ;;
    -e|--extend)
    EXTEND=$2
    DEFAULTEXTEND=FALSE
    shift
    ;;
    -w|--overwrite)
    OVERWRITE=TRUE
    ;;
    -l|--slurm)
    SLURM=TRUE
    ;;
    -u|--ucsc)
    UCSC=TRUE
    ;;
    --readset)
    READSET=TRUE
    ;;
    -n|--nosubmit)
    NOSUBMIT=TRUE
    ;;
    -h|--help)
    HELP=TRUE
    ;;
esac
shift # past argument or value
done

if [ $MUGQIC_DIR = ERROR ] || [ $RAP = ERROR ]
then
    echo -e "Error: Both -d MUGQIC_DIR and -r RAP_ID must be specified.\n\n"
    HELP=TRUE
fi

if [ ! -e $MUGQIC_DIR ]
then
    echo -e "Error: $MUGQIC_DIR not found.\n\n"
    HELP=TRUE
fi

if [ ! -d $MUGQIC_DIR ]
then
    echo -e "Error: --mugqicdir must be a directory.\n\n"
    HELP=TRUE
fi


if [ $HELP = TRUE ]
then
    cat <<EOF
This utility program scans the output of the MUGQIC genpipes pipeline
and generates bigwig files from the specified bam files. The scripts used
for generating the bigwig files are saved in the "jobs" directory within
the genpipes output and are submitted to SLURM. 

The generated bigwig files are saved in the tracks directory within the genpipes
output.
    
Usage: generate_bigwig.sh -d input_dir -r rap_id [OPTIONS]
Example: generate_bigwig.sh -d output/pipeline -r def-amnou -l -e 200
    
Mandatory parameters:
    -d, --mugqicdir=DIR   Root directory of genpipes ChIP pipeline output.
    -r, --rapid=RAP       Ressource allocation ID (Example: def-amnou)

Input parameters:
    --readset             Generate bigwigs from readset bam files rather than
                          sample bam files. (Default: Off)
    --inputsuffix         Suffix of the bam files to be converted.
                          (Default: .sorted.dup.bam for ChIP,
                           .sorted.mdup.hardClip.bam for RNA)
Output parameters:
    -w, --overwrite       Overwrite existing files.
    -u, --ucsc            Swith from ENSEMBL to UCSC chromosome names. 
                          (Default: Off) 
    -o, --outputsuffix    Suffix to be appended to generated bigwig files.
                          (Default: None)

Processing parameters:
    -e, --extend=LEN      Extend reads to this many base-pairs. (Default: 200)
    -l, --slurm           Use SLURM job manager to submit jobs. (Default: On)
    -n, --nosubmit        Generate job scripts, but do not submit them to the
                          job manager (Default: Off)
    -h, --help            Print this help message.
EOF

exit    
fi


# Switch the input file set depending on whether we're producing
# coverage for the readset or the sample files.
RNAJOBLIST=$MUGQIC_DIR/job_output/RnaSeq*
if [ ! -e $RNAJOBLIST ]
then
    if [ $READSET = TRUE ]
    then
        FILESET=$MUGQIC_DIR/alignment/*/*/*$INPUTSUFFIX
    else
        FILESET=$MUGQIC_DIR/alignment/*/*$INPUTSUFFIX    
    fi
else
    if [ $DEFAULTSUFFIX = TRUE ]
    then
        INPUTSUFFIX=.sorted.mdup.hardClip.bam
    fi
    
    if [ $DEFAULTEXTEND = TRUE ]
    then
        EXTEND=0
    fi    
    
    FILESET=$MUGQIC_DIR/alignment/*$INPUTSUFFIX
fi

# Output perl script for fixing chromosome names
cat <<'EOF' > fix_chr.pl
$F[2]="chr" . $F[2] unless ($F[2] eq "*");

if($F[6] ne "*" && $F[6] ne "=") { 
    $F[6]="chr".$F[6]
}

print join("\t", @F);
EOF

# Loop over all files in the readset.
jobids=""
for i in $FILESET
do
    # Infer the sample name, which should be the directory name
    # directly under the alignment folder.
    samplename=`echo $i | sed -e 's/.*\/alignment\/\([^/]*\)\/.*.bam/\1/'`

    # Generate a job only if the output file does not exist,
    # or if we should overwrite it:
    if [ ! -e "$MUGQIC_DIR/tracks/$samplename.bw" -o "$OVERWRITE" = "TRUE" ]
    then     
        # Make sure there's a jobs directory and name our job script.
        mkdir -p $MUGQIC_DIR/jobs
        script="$MUGQIC_DIR/jobs/$samplename.make_bigwig.sh"

        # Print shebang, since it's required by SLURM.
        cat <<EOF > $script
#!/bin/bash
EOF

        # Convert to UCSC naming convention if necessary.
        # If we're not, we'll generate a dummy intermediate file instead.
        if [ $UCSC = TRUE ]
        then
cat <<EOF >> $script
module load samtools
    # Script to add chr obtained from https://www.biostars.org/p/10062/
    # Add chr prefix to bam file.
    samtools view -H $i |
        perl -lpe 's/Mito/M/' |
        perl -lpe 's/SN:([I]+|I*VI*|I*XV*I*V*|[0-9]+|[XY]|M)\b/SN:chr\$1/' > $samplename.sam
    
    samtools view $i |
      perl -lpe 's/Mito/M/' |
      perl -lan fix_chr.pl >> $samplename.sam
    samtools view -b -h $samplename.sam > $samplename.bam
    samtools index -@ 16 $samplename.bam $samplename.bai
    rm $samplename.sam
EOF
        else
cat <<EOF >> $script
            ln -s $i $samplename.bam
            ln -s `echo $i | sed -e 's/bam$/bai/'` $samplename.bai
EOF
        fi

        # Finally, calculate coverages.
cat <<EOF >> $script
module load mugqic/python/2.7.12

mkdir -p $MUGQIC_DIR/tracks

bamCoverage -e $EXTEND --binSize 5 -p 16 --normalizeUsingRPKM \
    -b $samplename.bam \
    -o $MUGQIC_DIR/tracks/$samplename$OUTPUTSUFFIX.bw
rm $samplename.bam    
EOF
        
        # Launch the job using the correct scheduler.
        if [ $NOSUBMIT = FALSE ]
        then
            workdir=`pwd -P`
            if [ $SLURM = TRUE ]
            then
                jobid=`sbatch --time=6:00:00 --account=$RAP -J $script -N 1 --mem=32G --mincpus 16 -o $script.stdout -e $script.stderr -D $workdir $script `
                jobid=`echo $jobid | sed -e 's/Submitted batch job //'`
                jobids=$jobids:$jobid
            else
                jobids=$jobids:`qsub $script -o $script.stdout -e $script.stderr -d $workdir`
            fi
        fi
    fi
done

echo $jobids
