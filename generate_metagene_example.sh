#!/bin/bash
#SBATCH --time=04:00:00
#SBATCH --account=def-amnou
#SBATCH --mem=32G
#SBATCH --cpus-per-task 8
#SBATCH --error stderr.log
#SBATCH --output stdout.log

# Soumets ce script avec la commande:
#    sbatch -D `pwd` generate_metagene_example.sh


module load nixpkgs/16.09 gcc/7.3.0 r/3.6.1

# Tu vas avoir besoin d'installer deux librairies dans R pour faire fonctionner
# ce script: metagene2 et argparser.
# Tu peux les installer avec les lignes suivantes:
# install.packages("BiocManager")
# install.packages("argparser")
# BiocManager::install("metagene2")

# Dans un fichier bash, finir une ligne avec le symbole \ (backslash) indique 
# que la commande se poursuit à la ligne suivante. Ajoute des options au  
# metagene en rajoutant des lignes à la commande Rscript, et en mettant
# un backslash à la fin de toutes les lignes sauf la dernière.

# Mets le bon nom de région:
Rscript ~/mugqic_utils/generate_metagene.R -r TSS-NAMED_GENES-NON_OVERLAPPING-1000.txt \
#
# Inclus cette ligne si tu veux étendre les reads à 200nt
#    --extend 200 \
#
# Inclus cette ligne si tu veux changer les dimensions du PDF sortant
#    --width 14 --height 14 \
#
# Inclus cette ligne pour changer le nombre de processeurs en parrallèle.
# C'est inutile de mettre plus de processeur qu'il n'y a de bam.
# Il faut avoir au minimum 4Go de mémoire par processeur.
# Il ne faut pas oublier de changer le nombre de CPU et la mémoire dans l'en-tête SLURM.
#    --thread 8 \
#
# Inclus cette ligne pour changer les groupes et les facettes.
# Le groupe doit être une colonne dans le fichier readset.txt ou le fichier regions.
# La facette est une formule de type x~y, où x et y sont des colonnes des mêmes fichiers.
# Tu peux aussi mettre un . si tu ne veux qu'un seul axe de facettes sur ton graph.
#    --group Sample --facet "Antibody~WTMean_Q" \
#
# Inclus cette ligne pour sauver l'objet metagene après avoir produit le graphe.
# Tu peux ensuite l'ouvrir dans R pour le modifier.
#    --cache metagene.rds \
#
# Modifies cette ligne si la sortie du pipelien de mugqic n'est pas dans le
# sous-dossier output/pipeline.
#    --mugqic output/pipeline \
#
# Modifies cette ligne si tu veux utiliser un autre readset. Tu peux créer des 
# fichiers readset qui sont des sous-ensembles de tes échantillons pour faire
# des métagènes avec seulement certains échantillons.
#    --sample raw/readset.txt \
#
# Ajoute cette ligne pour avoir des graphs en décompte de reads bruts plutôt 
# qu'en RPM.
#    --nonorm \
#
# Par défaut le pipeline filtre les bams pour enlever les alignements ambigüs,
# ce qui fais que tu vas avoir une couverture à 0 dans les régions répétées.
# Tu peux utiliser les bam non-filtrés en utilisant l'option suivante:
#    --unfiltered \
#
# metagene2 calcule des intervalles de confiance des couvertures qui s'affichent
# comme une bande pâle autour de la ligne. Tu peux les enlever avec le flag
# suivant:
#    --noci \

# Exemple de ligne de commande complète:
#   Rscript ~/mugqic_utils/generate_metagene.R -r TSS-NAMED_GENES-NON_OVERLAPPING-1000.txt \
#   --thread 8 \
#   --cache metagene.rds \
#   --width 14 --height 14 \
#   --group Sample -facet "Antibody~WTMean_Q"
